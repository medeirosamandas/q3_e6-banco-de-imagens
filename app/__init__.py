from flask import Flask, request, jsonify, send_from_directory
import os
from flask.helpers import safe_join
from werkzeug.utils import secure_filename


FILES_DIRECTORY = './uploads'
MAX_CONTENT_LENGTH = 1024000
app = Flask(__name__)

if not os.path.exists(FILES_DIRECTORY):
            os.makedirs(FILES_DIRECTORY)


@app.post('/upload')
def upload():
    files = os.listdir(FILES_DIRECTORY)
    files_list = list(request.files)
    acceptable_formats = ['png', 'jpg', 'jpeg', 'gif']

    if int(request.headers['content-length']) > MAX_CONTENT_LENGTH:
        return {'msg': 'arquivo muito grande'}, 413

    for item in files_list:
        received_file = request.files[item]
        extension = received_file.content_type.split('/')[-1]
        if extension in acceptable_formats:
            filename = secure_filename(received_file.filename)
            if filename not in files:
                file_path = safe_join(FILES_DIRECTORY, filename)
                received_file.save(file_path)
                return jsonify({'msg': f'arquivo {filename} criado'}), 201
            return jsonify({'msg': 'Arquivo com o mesmo nome já existente'}), 409
        return jsonify({'msg': 'formato inválido'}), 415

    
@app.get('/files')
def list_files():
    files = os.listdir(FILES_DIRECTORY)
    return jsonify(files)


@app.get('/files/<string:tipo>')
def list_files_by_type(tipo):
    files = os.listdir(FILES_DIRECTORY)
    files_by_type = []
    for f in files:
        if f.split('.')[-1].lower() == tipo.lower():
            files_by_type.append(f)

    return jsonify(files_by_type), 200


@app.get('/download/<string:file_name>')
def download(file_name):
    files = os.listdir(FILES_DIRECTORY)


    if file_name.lower() in files:
        return send_from_directory('../uploads', file_name, as_attachment=True), 200

    return jsonify({'msg': 'arquivo não encontrado'}), 404


@app.get('/download-zip')
def download_dir_as_zip():
    files = os.listdir(FILES_DIRECTORY)
    if files == []:
        return {"msg": "nenhum arquivo para ser baixado"}, 404 
    
    os.system("zip files uploads/*")
    return send_from_directory('../', "files.zip", as_attachment=True), 200


@app.get('/download-zip/<string:file_extension>')
def download_zip_with_fileextension(file_extension):
    files = os.listdir(FILES_DIRECTORY)
    if files == []:
        return {"msg": "nenhum arquivo para ser baixado"}, 404 
    
    os.system(f"zip {file_extension} uploads/*.{file_extension}")
    return send_from_directory('../', f"{file_extension}.zip", as_attachment=True), 200
